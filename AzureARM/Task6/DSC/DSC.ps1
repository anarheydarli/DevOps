Configuration Main {


    Param ( 
        [string] $nodeName, 
        [string] $site1, 
        [string] $site2            
    )

    Import-DscResource -ModuleName PSDesiredStateConfiguration
    Import-DscResource -ModuleName xPSDesiredStateConfiguration
    Import-DscResource -ModuleName xWebAdministration

    Node $nodeName {
    

        File WebFolder1 {
            Type            = "Directory"
            DestinationPath = "C:\inetpub\coreweb1"
            Ensure          = "Present"
        }   

        File WebFolder2 {
            Type            = "Directory"
            DestinationPath = "C:\inetpub\coreweb2"
            Ensure          = "Present"
        }   
        
        xRemoteFile Downloadindexhtml1 {            	
            DestinationPath = "C:\coreweb1\firstindex.html"
            Uri             = $site1
            MatchSource     = $true
        }

        xRemoteFile Downloadindexhtml2 {            	
            DestinationPath = "C:\coreweb2\secondindex.html"
            Uri             = $site2
            MatchSource     = $true
        }
        
        WindowsFeature IIS {
            Ensure = "Present"
            Name   = "Web-Server"
        }

        WindowsFeature ASPNet45 {
            Name      = "Web-Asp-Net45"
            Ensure    = "Present"
            DependsOn = '[WindowsFeature]IIS'
        }
 
        WindowsFeature Management {
            Name      = 'Web-Mgmt-Console'
            Ensure    = 'Present'
            DependsOn = '[WindowsFeature]IIS'
        }

        xWebAppPool AppPool {
            Name         = 'WebAppPool'
            autoStart    = $true
            Ensure       = 'Present' 
            identityType = 'LocalSystem'
            startMode    = 'AlwaysRunning'
            State        = 'Started'
            DependsOn    = '[WindowsFeature]IIS'
        }

        xWebsite DefaultSite {
            Ensure       = "Present"
            Name         = "Default Web Site"
            State        = "Stopped"
            PhysicalPath = "C:\inetpub\wwwroot"
            DependsOn    = "[WindowsFeature]IIS"
        }

        xWebsite WebSite1 {
            Name            = 'WebSites1'
            ApplicationPool = 'WebAppPool'
            DefaultPage     = 'firstindex.html'
            BindingInfo     = @(MSFT_xWebBindingInformation {
                    Protocol  = "HTTP"
                    Port      = 8080
                    IPAddress = '*'

                })
            Ensure          = 'Present'
            PhysicalPath    = 'C:\coreweb1'
            State           = 'Started'
            DependsOn       = '[xWebAppPool]AppPool'
        }

        xWebsite WebSite2 {
            Name            = 'WebSites2'
            ApplicationPool = 'WebAppPool'
            DefaultPage     = 'secondindex.html'
            BindingInfo     = @(MSFT_xWebBindingInformation {
                    Protocol  = "HTTP"
                    Port      = 8081
                    IPAddress = '*'
                })
            Ensure          = 'Present'
            PhysicalPath    = 'C:\coreweb2'
            State           = 'Started'
            DependsOn       = '[xWebAppPool]AppPool'
        }
    }
}