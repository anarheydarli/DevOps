Configuration Main
{

    Param ( [string] $nodeName,
            [string] $tempFileLocation            
    )

    Import-DscResource -ModuleName PSDesiredStateConfiguration
    Import-DscResource -ModuleName xPSDesiredStateConfiguration

    File ScriptsFolder {
        Type            = "Directory"
        DestinationPath = "C:\Scripts"
        Ensure          = "Present"
    }   

    xRemoteFile DownloadPackage {            	
      DestinationPath = "C:\Scripts\test1.ps1"
      Uri             = $tempFileLocation
      MatchSource     = $true
    }
}