param(
    [string][Parameter(Mandatory=$True)] $fullPathToJsonFile
)

Login-AzureRmAccount

Save-AzureRmContext -Path $fullPathToJsonFile

Import-AzureRmContext -Path $fullPathToJsonFile