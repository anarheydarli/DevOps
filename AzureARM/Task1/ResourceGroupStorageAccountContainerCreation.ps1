﻿# Azure Resourcce Group & Storage Account & Container Creation & Uploading Blobs 

param(
    [string] $resourceGroup = "AnarRG",
    [string] $location = "westus",
    [string] $storageAccountName = "storageanar",
    [string] $containerName = "newblobcontainer",
    [string] $localFileDirectory = "$HOME\Desktop\DevOpsTasks\AzureARM\Storage\Files\"
)

Connect-AzureRmAccount

# You can list available location with a command below:
# Get-AzureRmLocation

# put resource group in a variable so you can use the same group name going forward,
# without hardcoding it repeatedly
New-AzureRmResourceGroup -Name $resourceGroup -Location $location

# Get-AzureRmLocation | select Location 

New-AzureRmStorageAccount -ResourceGroupName $resourceGroup `
  -Name $storageAccountName `
  -Location $location `
  -SkuName Standard_LRS `
  -Kind StorageV2

$storageAccountKey = (Get-AzureRmStorageAccountKey -ResourceGroupName $resourceGroup -Name $storageAccountName).Value[0]
$ctx = (New-AzureStorageContext -StorageAccountName $storageAccountName -StorageAccountKey $storageAccountKey)

# Get-AzureRMStorageAccount | Select StorageAccountName, Location
New-AzureStorageContainer -Name $containerName -Context $ctx -Permission blob

# Upload a file
Get-ChildItem -Path $localFileDirectory -Recurse -File | `
Set-AzureStorageBlobContent  `
  -Container $containerName `
  -Context $ctx 

# List the blobs in a container
Get-AzureStorageBlob -Container $ContainerName -Context $ctx | Select-Object Name

# Clean up resources
# Remove-AzureRmResourceGroup -Name $resourceGroup