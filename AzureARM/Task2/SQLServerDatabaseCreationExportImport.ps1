<# 
  In this script, we are going to create two or more SQL Servers and several databases in one of them. 
  For testing purposes, we are creating test tables in these databases and inserting  rows to those databases. 
  Then we are exporting databases from one SQL Server and importing them to another SQL Server. 
#>
param(
    [string][Parameter(Mandatory=$True)]                        $rgName,
    [string][Parameter(Mandatory=$True)]                        $locationName,
    [string][Parameter(Mandatory=$True)]                        $storageAccountName,
    [string][Parameter(Mandatory=$True)]                        $containerName,
    [string][Parameter(Mandatory=$True)]                        $adminLogin,
    [string][Parameter(Mandatory=$True)]                        $plainTextPassword,
    [string[]][Parameter(Mandatory=$True)]                      $sqlServerUniqueName,
    [string[]][Parameter(Mandatory=$True)]                      $sqlDatabaseName,             
    [string][Parameter(Mandatory=$True)]                        $firewallRuleName,
    [string][Parameter(Mandatory=$True)]                        $databaseExportFileName
)
$storageAccountName   = $storageAccountName.ToLower()
$sqlServerUniqueName  = $sqlServerUniqueName.ToLower()

Connect-AzureRmAccount

# Converting plain text passwword to secure string.
$securePassword = (ConvertTo-SecureString -AsPlainText($plainTextPassword) -Force)

# The ip address range that you want to allow to access your server
$startip = "0.0.0.0"
$endip = "255.255.255.255"

# Create a resource group
$resourceGroup = New-AzureRmResourceGroup -Name $rgName -Location $locationName

# Create a SQL server İnstance with a system wide unique server name
foreach ($sqlsrv in $sqlServerUniqueName) {
  $sqlInstance = New-AzureRmSqlServer -ResourceGroupName $rgName `
  -ServerName "$sqlsrv" `
  -Location $locationName `
  -SqlAdministratorCredentials $(New-Object -TypeName System.Management.Automation.PSCredential `
  -ArgumentList $adminLogin, $securePassword);
}

# Create a variables for SQL servers and databases
$sqlServerName01 = (Get-AzureRmSqlServer -ResourceGroupName $rgName).serverName[0]
$sqlServerName02 = (Get-AzureRmSqlServer -ResourceGroupName $rgName).serverName[1]
$sqlInstanceFullName01 = (Get-AzureRmSqlServer -ResourceGroupName $rgName).FullyQualifiedDomainName[0]

# Create a server firewall rule that allows access from the specified IP range
foreach ($frw in $sqlServerUniqueName) {
   $serverFirewallRule = New-AzureRmSqlServerFirewallRule -ResourceGroupName $rgName `
  -ServerName "$frw" `
  -FirewallRuleName $firewallRuleName -StartIpAddress $startip -EndIpAddress $endip
}

# Create a blank three databases with an S0 performance level
foreach ($db in $sqlDatabaseName) {
    $database = New-AzureRmSqlDatabase  -ResourceGroupName $rgName `
    -ServerName $sqlServerName01 `
    -DatabaseName "$db" `
    -RequestedServiceObjectiveName "S0" `
    -SampleName "AdventureWorksLT"
}

# Create a variablew which contains T-SQL to create the table 
$TableStatement = 'CREATE TABLE TestTable (Name varchar(25) NOT NULL, LastName char(25) NOT NULL, Age varchar(400) NOT NULL, Roles varchar(400) NOT NULL)'

# Call the databases and populate tables 
foreach ($sqlddl in $sqlDatabaseName) {
    Invoke-Sqlcmd -Database "$sqlddl" -ServerInstance $sqlInstanceFullName01 -Username $adminLogin -Password $plainTextPassword -OutputSqlErrors $True -Query $TableStatement
}

# Insert data into newly created tables
$TableData = "INSERT INTO TestTable VALUES ('Anar','Heydarli','Oracle DBA','28')"
foreach ($sqldml in $sqlDatabaseName) {
Invoke-Sqlcmd -Database "$sqldml" -ServerInstance  $sqlInstanceFullName01 -Username $adminLogin -Password $plainTextPassword -OutputSqlErrors $True -Query $TableData
}

# Storage Account creation
New-AzureRmStorageAccount -ResourceGroupName $rgName `
  -Name $storageAccountName `
  -Location $locationName `
  -SkuName Standard_LRS `
  -Kind StorageV2

# Creating storage account related variables
$storageAccountKey = (Get-AzureRmStorageAccountKey -ResourceGroupName $rgName -Name $storageAccountName).Value[0]
$ctx = (New-AzureStorageContext -StorageAccountName $storageAccountName -StorageAccountKey $storageAccountKey)

# Creating a blob container. 
New-AzureStorageContainer -Name $containerName -Context $ctx -Permission blob

# Creating variables for SQL Server Export statement
$bacpacUri = ((Get-AzureStorageContainer -Name $containerName -Context $ctx).CloudBlobContainer.Uri.AbsoluteUri)+"/$databaseExportFileName"
$storageKeytype = "StorageAccessKey"

# Exporting databases from the first SQL Server
foreach ($db in $sqlDatabaseName) {
    $exportRequest = New-AzureRmSqlDatabaseExport -ResourceGroupName $rgName -ServerName $sqlServerName01 `
    -DatabaseName "$db" -StorageKeytype $storageKeytype -StorageKey $storageAccountKey -StorageUri $bacpacUri `
    -AdministratorLogin $adminLogin -AdministratorLoginPassword $securePassword
}

# Export Status
$exportStatus = Get-AzureRmSqlDatabaseImportExportStatus -OperationStatusLink $exportRequest.OperationStatusLink
[Console]::Write("Exporting")
while ($exportStatus.Status -eq "InProgress")
{
    Start-Sleep -s 10
    $exportStatus = Get-AzureRmSqlDatabaseImportExportStatus -OperationStatusLink $exportRequest.OperationStatusLink
    [Console]::Write(".")
}
[Console]::WriteLine("")
$exportStatus

# Import database to the second SQL Server
foreach ($db in $sqlDatabaseName) {
  $importRequest =  New-AzureRmSqlDatabaseImport -ResourceGroupName $rgName -ServerName $sqlServerName02 `
  -DatabaseName "$db" -StorageKeyType $storageKeytype -StorageKey $storageAccountKey -StorageUri $bacpacUri `
  -AdministratorLogin $adminLogin -AdministratorLoginPassword $securePassword -Edition Standard -ServiceObjectiveName S0 -DatabaseMaxSizeBytes 5000000
}

# Import Status
$importStatus = Get-AzureRmSqlDatabaseImportExportStatus -OperationStatusLink $importRequest.OperationStatusLink
[Console]::Write("Importing")
while ($importStatus.Status -eq "InProgress")
{
    Start-Sleep -s 10
    $importStatus = Get-AzureRmSqlDatabaseImportExportStatus -OperationStatusLink $importRequest.OperationStatusLink
    [Console]::Write(".")
}
[Console]::WriteLine("")
$importStatus