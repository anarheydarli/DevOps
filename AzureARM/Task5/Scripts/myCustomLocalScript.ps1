param([string] $dir, 
    [int32] $size
)
$files = Get-ChildItem $dir
foreach ($file in $files) {
    if ($file.length -gt $size) {
        Write-Output $file >> "C:\Scripts\result.txt"
    }
}