Configuration Main {


    Param ( 
        [string] $nodeName, 
        [string] $site1, 
        [string] $site2            
    )

    Import-DscResource -ModuleName PSDesiredStateConfiguration
    Import-DscResource -ModuleName xPSDesiredStateConfiguration
    Import-DscResource -ModuleName xWebAdministration
    Import-DscResource -ModuleName xStorage
    Import-DSCResource -ModuleName StorageDsc

    Node $nodeName {
  

        File WebFolder1 {
            Type            = "Directory"
            DestinationPath = "C:\inetpub\coreweb1"
            Ensure          = "Present"
        }   

        xRemoteFile Downloadindexhtml1 {            	
            DestinationPath = "C:\coreweb1\firstindex.html"
            Uri             = $site1
            MatchSource     = $true
        }
      
        WindowsFeature IIS {
            Ensure = "Present"
            Name   = "Web-Server"
        }

        WindowsFeature ASPNet45 {
            Name      = "Web-Asp-Net45"
            Ensure    = "Present"
            DependsOn = '[WindowsFeature]IIS'
        }

        WindowsFeature Management {
            Name      = 'Web-Mgmt-Console'
            Ensure    = 'Present'
            DependsOn = '[WindowsFeature]IIS'
        }

        xWebAppPool AppPool {
            Name         = 'WebAppPool'
            autoStart    = $true
            Ensure       = 'Present' 
            identityType = 'LocalSystem'
            startMode    = 'AlwaysRunning'
            State        = 'Started'
            DependsOn    = '[WindowsFeature]IIS'
        }

        xWebsite DefaultSite {
            Ensure       = "Present"
            Name         = "Default Web Site"
            State        = "Stopped"
            PhysicalPath = "C:\inetpub\wwwroot"
            DependsOn    = "[WindowsFeature]IIS"
        }

        xWebsite WebSite1 {
            Name            = 'WebSites1'
            ApplicationPool = 'WebAppPool'
            DefaultPage     = 'firstindex.html'
            BindingInfo     = @(MSFT_xWebBindingInformation {
                    Protocol  = "HTTP"
                    Port      = 80
                    IPAddress = '*'

                })
            Ensure          = 'Present'
            PhysicalPath    = 'C:\coreweb1'
            State           = 'Started'
            DependsOn       = '[xWebAppPool]AppPool'
        }

        xWaitforDisk Disk2
        {
            DiskId = 2
            RetryIntervalSec = 60
            RetryCount = 60
        }

        xDisk FVolume
        {
            DiskId = 2
            DriveLetter = 'F'
            FSLabel = 'Data'
            DependsOn = '[xWaitforDisk]Disk2'
        }
    } 
}
