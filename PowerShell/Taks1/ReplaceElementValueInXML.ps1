<# 
This script about: 
Replace several element values in xml file 
and make ten copies of this file in the specified directory
#>


param(
    [Parameter(Mandatory = $true)] [string] $xmlFile,
    [Parameter(Mandatory = $true)] [string] $saveToPath
)


for ($i = 1; $i -le 10; $i++) {
    
    $var = Get-Random
    $xml = New-Object XML
    $xml.Load($xmlfile)
    $element = ($xml.SelectNodes("//th"))
    $element[0].InnerText = "Title_$var"
    $element[1].InnerText = "Artist_$var"
    
    $xml.Save("$saveToPath\test$i.xml")
}
