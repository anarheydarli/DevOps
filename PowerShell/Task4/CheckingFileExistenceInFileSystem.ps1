<#
This script checks file existence in the file system, 
and if doesn't exist then downloads it from some URL.
#>

param(
    [Parameter(Mandatory = $true)] [string] $fullFilePath,
    [Parameter(Mandatory = $true)] [string] $url
)

if (Test-Path $fullFilePath -PathType Leaf) {
    Write-Host "The JPG file exists in the specified directory."
}
else {
        Write-Host "The JPG file does not exist in the specified directory."
        Write-Host "The JPG file is downloading..."
        Invoke-WebRequest $url -OutFile $fullFilePath
        #$wc = New-Object System.Net.WebClient
        #$wc.DownloadFile($url, "$fullFilePath")
}