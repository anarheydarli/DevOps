<#
This script checks if a variable has any value, 
and if doesn't has then defines a value for the variable and prints this value.
#>

param(
    [Parameter(Mandatory = $true)] [string] $newVariableValue 
)

$variableName = $newVariableValue.TrimStart().TrimEnd()    

if (!$variableName) { 
    Write-Host 'Variable is null or empty.'
}
elseif ($variableName) {
    Write-Host 'Variable is NOT null.'
    Write-Host "New variable value is: $variableName"
}