<#
This is a PowerShell which contains multiple functions. 
#>

Function Get-SumValues ($a, $b, $c)
    {
        Write-Host "Entered values are summing up and it is equal to: " 
        [int32]$a + [int32]$b + [int32]$c
    }

Function Get-SubtractValues ($a, $b, $c)
    {
        Write-Host "Entered values are subtracting and it is equal to: "
        [int32]$a - [int32]$b - [int32]$c
    }

    
Function Get-DevideValues ($a, $b, $c)
    {
        Write-Host "Entered values are deviding and it is equal to: "
        [float]$a / [float]$b / [float]$c
    }


Function Get-MultiplyValues ($a, $b, $c)
    {
        Write-Host "Entered values are multiplying and it is equal to: "
        [int32]$a * [int32]$b * [int32]$c
    }

    