<#
These script uses PowerShell module
which contains multiple functions. 
#>

Param
(    
    [Parameter(Mandatory = $true)] [Int32] $value1,
        
    [Parameter(Mandatory = $true)] [Int32] $value2,
        
    [Parameter(Mandatory = $true)] [Int32] $value3
)

Import-Module $HOME\Desktop\DevOpsTasks\PowerShell\Task8\myPowerShellModule.psm1

Get-SumValues -a $value1 -b $value3 -c $value2
Get-SubtractValues -a $value3 -b $value1 -c $value2
Get-MultiplyValues -a $value2 -b $value1 -c $value3
Get-DevideValues -a $value1 -b $value2 -c $value3