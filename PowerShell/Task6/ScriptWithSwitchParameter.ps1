<#
Command inside <if> condition executes if switch
parameter returns true.
#>

param([switch]$Option)

if ($Option) {
    Get-Disk
}