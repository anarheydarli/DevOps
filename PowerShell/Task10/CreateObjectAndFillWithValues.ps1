<#
In this script we are creating custom object
and populating with properties or methods. 
#>

$myObject = New-Object -TypeName psobject 

$myObject | Add-Member -MemberType NoteProperty -Name Name -Value Anar
$myObject | Add-Member -MemberType NoteProperty -Name Surname -Value Heydarli
$myObject | Add-Member -MemberType NoteProperty -Name Age -Value 28
$myObject | Add-Member -MemberType NoteProperty -Name Height -Value 170cm