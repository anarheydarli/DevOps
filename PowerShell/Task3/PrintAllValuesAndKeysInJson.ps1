<# 
This script about itirate through json file 
and print out all keys and values in json file.
#>

param(
    [Parameter(Mandatory = $true)] [string] $fullFilePath,
)

$jsonFile = (Get-Content $fullFilePath -Raw) | ConvertFrom-Json
$jsonFile.PsObject.BaseObject | 
ForEach-Object {
    Write-Host ($jsonFile.PsObject.BaseObject | Get-Member -MemberType NoteProperty).Name[2] ":"  $_.Rgname
    Write-Host ($jsonFile.PsObject.BaseObject | Get-Member -MemberType NoteProperty).Name[1] ":"  $_.Name
    Write-Host ($jsonFile.PsObject.BaseObject | Get-Member -MemberType NoteProperty).Name[0] ":"  $_.Appname
 } | Format-List 