<# 
This script about Update value in json file 
and save file in the specified directory
#>

param(
    [Parameter(Mandatory = $true)] [string] $pathToJson
)

$a = Get-Content $pathToJson -raw | ConvertFrom-Json
$a.glossary.GlossDiv.GlossList.GlossEntry.SortAs = "OMPL"
$a | ConvertTo-Json -Depth 100 | set-content $pathToJson
