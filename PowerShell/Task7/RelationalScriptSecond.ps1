<#
These Relational scripts call each other
and make some calculations based on passed parameters. 
#>

Param
(    
    [Parameter(Mandatory = $true)] [Int32] $value1,
        
    [Parameter(Mandatory = $true)] [Int32] $value2,
        
    [Parameter(Mandatory = $true)] [Int32] $value3
)

. $HOME\Desktop\PowerShellTemporary\Task7\RelationalScriptFirst.ps1 -value1 $value1 -value3 $value3 -value2 $value2