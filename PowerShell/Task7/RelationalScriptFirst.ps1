<#
These Relational scripts call each other
and make some calculations based on passed parameters. 
#>

Param
(    
    [Parameter(Mandatory = $true)] [Int32] $value1,
        
    [Parameter(Mandatory = $true)] [Int32] $value2,
        
    [Parameter(Mandatory = $true)] [Int32] $value3
)

Function calculation ($a, $b, $c)
    {
         [int32]$a + [int32]$b + [int32]$c
    }

Write-Host "Entered values are summing up and it is equal to: "(calculation -a $value1 -b $value2 -c $value3)